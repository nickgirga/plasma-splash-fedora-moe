# plasma-splash-fedora-moe
Fork of [Plasma Splash Screen for Archlinux, Moe Themed](https://github.com/perigoso/plasma-splash-arch-moe)

Updated with SVGs to accommodate high resolution displays and a slightly darker background to really make the colors pop. You can find the Arch variations of the SVG versions [here](https://github.com/nickgirga/plasma-splash-arch-moe). After moving to Fedora, I wanted to keep using the splash screen, so I created a logo for Fedora. That's what this project page is. It's a dedicated repository for the Fedora variation of [Plasma Splash Screen for Archlinux, Moe Themed](https://github.com/perigoso/plasma-splash-arch-moe).

### Preview
![Preview](FedoralinuxMoe/contents/previews/splash.png)

## Installation

Copy the folder `FedoralinuxMoe` to `~/.local/share/plasma/look-and-feel/`

## Set the Splash Screen

To set this splash screen as your plasma splash screen:
- Open KDE's System Settings
- Click `Appearance`
- Click `Splash Screen`
- Choose `Fedora Linux (Moe)`
- Click `Apply`

## Acknowledgements
- [Moe Theme](https://gitlab.com/jomada/moe-theme)
- [YAPLASS ArchLinux](https://www.opencode.net/mrmaire)
- [Original Plasma Splash Arch Moe](https://github.com/perigoso/plasma-splash-arch-moe)
